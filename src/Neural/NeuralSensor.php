<?php

namespace Mmatweb\Neural\Neural;

class NeuralSensor extends AbstractNeural
{
    /** @var bool */
    private $calibrateFalse;
    /** @var bool */
    private $calibrateTrue;

    /** @var Synapse */
    private $synapse1;

    /** @var Axon */
    private $axon1;

    public function __construct(bool $calibrateFalse, bool $calibrateTrue)
    {
        $this->synapse1 = new Synapse($this);
        $this->axon1 = new Axon();

        $this->calibrateFalse = $calibrateFalse;
        $this->calibrateTrue = $calibrateTrue;

        $this->synapse1->onReceive(function (bool $signal, self $neuralSensor) {
            $neuralSensor->axon1->sendSignal($signal ? $this->calibrateTrue : $this->calibrateFalse);
        });
    }

    public function getSynapse1(): Synapse
    {
        return $this->synapse1;
    }

    public function getAxon1(): Axon
    {
        return $this->axon1;
    }
}
