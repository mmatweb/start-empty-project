<?php

namespace Mmatweb\Neural\Neural;

use Mmatweb\Neural\Exceptions\NeuralOrderSignalException;

class NeuralMemoryManager extends AbstractNeural
{
    /** @var Synapse */
    private $synapse1;
    /** @var Synapse */
    private $synapse2;
    /** @var Synapse */
    private $synapse3;

    /** @var Axon */
    private $axon1;
    /** @var Axon */
    private $axon2;
    /** @var Axon */
    private $axon3;

    /** @var bool */
    private $memorySignal2;
    /** @var bool */
    private $memoryFalse;
    /** @var bool */
    private $memoryTrue;

    public function __construct()
    {
        $this->synapse1 = new Synapse($this);
        $this->synapse2 = new Synapse($this);
        $this->synapse3 = new Synapse($this);

        $this->axon1 = new Axon();
        $this->axon2 = new Axon();
        $this->axon3 = new Axon();

        $this->synapse1->onReceive(function (bool $signal, self $neuralSensor) {
            if ((true === $signal && null === $neuralSensor->memoryTrue) ||
                (false === $signal && null === $neuralSensor->memoryFalse)) {
                $neuralSensor->axon1->sendSignal(false);

                return;
            }
            if (false === $signal && null !== $this->memoryFalse) {
                $neuralSensor->axon2->sendSignal($this->memoryFalse);

                return;
            }

            $neuralSensor->axon3->sendSignal($this->memoryTrue);
        });

        $this->synapse2->onReceive(function (bool $signal, self $neuralSensor) {
            $neuralSensor->memorySignal2 = $signal;
        });

        $this->synapse3->onReceive(function (bool $signal, self $neuralSensor) {
            if (null === $neuralSensor->memorySignal2) {
                throw new NeuralOrderSignalException();
            }

            if (false === $neuralSensor->memorySignal2) {
                $neuralSensor->memoryFalse = !$signal;
                $neuralSensor->axon2->sendSignal($this->memoryFalse);
                $neuralSensor->hasLearn();

                return;
            }

            $neuralSensor->memoryTrue = !$signal;
            $neuralSensor->axon3->sendSignal($this->memoryTrue);
            $neuralSensor->hasLearn();
        });
    }

    public function getSynapse1(): Synapse
    {
        return $this->synapse1;
    }

    public function getSynapse2(): Synapse
    {
        return $this->synapse2;
    }

    public function getSynapse3(): Synapse
    {
        return $this->synapse3;
    }

    public function getAxon1(): Axon
    {
        return $this->axon1;
    }

    public function getAxon2(): Axon
    {
        return $this->axon2;
    }

    public function getAxon3(): Axon
    {
        return $this->axon3;
    }

    private function hasLearn(): void
    {
        $this->memorySignal2 = null;
        if (null !== $this->memoryFalse && null !== $this->memoryTrue) {
            $this->synapse2->close();
            $this->synapse3->close();
        }
    }

    public function __debugInfo()
    {
        return [
            'memorySignal2' => $this->memorySignal2,
            'memoryFalse' => $this->memoryFalse,
            'memoryTrue' => $this->memoryTrue,
        ];
    }
}
