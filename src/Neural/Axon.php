<?php

namespace Mmatweb\Neural\Neural;

use Mmatweb\Neural\Interfaces\AxonInterface;
use Mmatweb\Neural\Interfaces\SynapseInterface;

class Axon implements AxonInterface
{
    /** @var SynapseInterface[] */
    private $synapses = [];

    public function sendSignal(bool $signal): void
    {
        foreach ($this->synapses as $synapse) {
            $synapse->receive($signal);
        }
    }

    public function attachSynapses(SynapseInterface ...$synapses): void
    {
        foreach ($synapses as $synapse) {
            if (\in_array($synapse, $this->synapses, true)) {
                continue;
            }

            $this->synapses[] = $synapse;
        }
    }

    public function detachSynapses(SynapseInterface ...$synapses): void
    {
        foreach ($synapses as $synapse) {
            if ($k = array_search($synapse, $this->synapses, true)) {
                unset($this->synapses[$k]);
            }
        }
    }
}
