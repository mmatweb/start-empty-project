<?php

namespace Mmatweb\Neural\Neural;

use Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined;
use Mmatweb\Neural\Interfaces\NeuralInterface;
use Mmatweb\Neural\Interfaces\SynapseInterface;

class Synapse implements SynapseInterface
{
    /** @var bool */
    private $isOpen = true;
    /** @var callable */
    private $onReceive;
    /** @var NeuralInterface */
    private $neural;
    /** @var null|bool */
    private $lastSignal;

    public function __construct(NeuralInterface $neural)
    {
        $this->neural = $neural;
    }

    public function open(): void
    {
        $this->isOpen = true;
    }

    public function close(): void
    {
        $this->isOpen = false;
    }

    public function isOpen(): bool
    {
        return $this->isOpen;
    }

    public function onReceive(callable $onReceive): void
    {
        $this->onReceive = $onReceive;
    }

    /**
     * @param bool $signal
     *
     * @throws SynapseOnReceiveIsNotDefined
     */
    public function receive(bool $signal): void
    {
        if (!$this->isOpen()) {
            return;
        }

        $this->lastSignal = $signal;
        $this->neural->setLastActiveSynapse($this);

        if (!$this->onReceive) {
            throw new SynapseOnReceiveIsNotDefined();
        }

        ($this->onReceive)($signal, $this->neural);
    }

    public function getLastSignal(): ?bool
    {
        return $this->lastSignal;
    }
}
