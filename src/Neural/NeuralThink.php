<?php

namespace Mmatweb\Neural\Neural;

class NeuralThink extends AbstractNeural
{
    /** @var Synapse */
    private $synapse1;
    /** @var Synapse */
    private $synapse2;
    /** @var Synapse */
    private $synapse3;
    /** @var Synapse */
    private $synapse4;
    /** @var Synapse */
    private $synapse5;

    /** @var Axon */
    private $axon1;
    /** @var Axon */
    private $axon2;
    /** @var Axon */
    private $axon3;
    /** @var Axon */
    private $axon4;

    public function __construct()
    {
        $this->synapse1 = new Synapse($this);
        $this->synapse2 = new Synapse($this);
        $this->synapse3 = new Synapse($this);
        $this->synapse4 = new Synapse($this);
        $this->synapse5 = new Synapse($this);

        $this->axon1 = new Axon();
        $this->axon2 = new Axon();
        $this->axon3 = new Axon();
        $this->axon4 = new Axon();

        $this->synapse1->onReceive(function (bool $signal, self $neuralThink) {
            $neuralThink->axon1->sendSignal($signal);
        });

        $this->synapse2->onReceive(function (bool $signal, self $neuralThink) {
            $neuralThink->axon2->sendSignal($signal);
        });

        $this->synapse3->onReceive(function (bool $signal, self $neuralThink) {
            if ($this->getLastActiveSynapse() === $this->synapse4 ||
                $this->getLastActiveSynapse() === $this->synapse5
            ) {
                return;
            }
            $neuralThink->axon3->sendSignal($neuralThink->synapse1->getLastSignal());
            $neuralThink->axon4->sendSignal($signal);
        });

        $this->synapse4->onReceive(function (bool $signal, self $neuralThink) {
            $neuralThink->axon2->sendSignal($signal);
        });
        $this->synapse4->close();

        $this->synapse5->onReceive(function (bool $signal, self $neuralThink) {
            $neuralThink->axon2->sendSignal($signal);
        });
        $this->synapse5->close();
    }

    public function activateSynapse4()
    {
        $this->synapse4->open();
    }

    public function activateSynapse5()
    {
        $this->synapse5->open();
    }

    public function deactivateSynapse3()
    {
        $this->synapse3->close();
    }

    public function getSynapse1(): Synapse
    {
        return $this->synapse1;
    }

    public function getSynapse2(): Synapse
    {
        return $this->synapse2;
    }

    public function getSynapse3(): Synapse
    {
        return $this->synapse3;
    }

    public function getSynapse4(): Synapse
    {
        return $this->synapse4;
    }

    public function getSynapse5(): Synapse
    {
        return $this->synapse5;
    }

    public function getAxon1(): Axon
    {
        return $this->axon1;
    }

    public function getAxon2(): Axon
    {
        return $this->axon2;
    }

    public function getAxon3(): Axon
    {
        return $this->axon3;
    }

    public function getAxon4(): Axon
    {
        return $this->axon4;
    }
}
