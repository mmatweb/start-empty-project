<?php

namespace Mmatweb\Neural\Neural;

use Mmatweb\Neural\Interfaces\NeuralInterface;
use Mmatweb\Neural\Interfaces\SynapseInterface;

abstract class AbstractNeural implements NeuralInterface
{
    /** @var null|SynapseInterface */
    private $lastActiveSynapse;

    public function setLastActiveSynapse(SynapseInterface $synapse): void
    {
        $this->lastActiveSynapse = $synapse;
    }

    public function getLastActiveSynapse(): ?SynapseInterface
    {
        return $this->lastActiveSynapse;
    }
}
