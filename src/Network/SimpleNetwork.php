<?php

namespace Mmatweb\Neural\Network;


use Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined;
use Mmatweb\Neural\Neural\Axon;
use Mmatweb\Neural\Neural\NeuralMemoryManager;
use Mmatweb\Neural\Neural\NeuralSensor;
use Mmatweb\Neural\Neural\NeuralThink;
use Mmatweb\Neural\Neural\Synapse;

class SimpleNetwork
{
    /** @var NeuralThink */
    private $neuralThink;

    /**
     * SimpleNetwork constructor.
     * @param bool $calibrateFalse
     * @param bool $calibrateTrue
     * @throws SynapseOnReceiveIsNotDefined
     */
    public function __construct(bool $calibrateFalse, bool $calibrateTrue)
    {
        $this->train($calibrateFalse, $calibrateTrue);
    }

    /**
     * @param bool $calibrateFalse
     * @param bool $calibrateTrue
     * @throws SynapseOnReceiveIsNotDefined
     */
    public function train(bool $calibrateFalse, bool $calibrateTrue)
    {
        $this->neuralThink = new neuralThink();

        $memoryManager = new NeuralMemoryManager();
        $this->neuralThink->getAxon1()->attachSynapses($memoryManager->getSynapse1());
        $this->neuralThink->getAxon3()->attachSynapses($memoryManager->getSynapse2());
        $this->neuralThink->getAxon4()->attachSynapses($memoryManager->getSynapse3());
        $memoryManager->getAxon1()->attachSynapses($this->neuralThink->getSynapse2());
        $memoryManager->getAxon2()->attachSynapses($this->neuralThink->getSynapse4());
        $memoryManager->getAxon3()->attachSynapses($this->neuralThink->getSynapse5());

        $neuralSensor = new NeuralSensor($calibrateFalse, $calibrateTrue);
        $this->neuralThink->getAxon2()->attachSynapses($neuralSensor->getSynapse1());
        $neuralSensor->getAxon1()->attachSynapses($this->neuralThink->getSynapse3());

        $this->neuralThink->getSynapse1()->receive(false);
        $this->neuralThink->getSynapse1()->receive(true);

        var_dump($memoryManager);

        $this->neuralThink->deactivateSynapse3();
        $this->neuralThink->activateSynapse4();
        $this->neuralThink->activateSynapse5();
    }

    public function getInput(): Synapse
    {
        return $this->neuralThink->getSynapse1();
    }

    public function getOutput(): Axon
    {
        return $this->neuralThink->getAxon2();
    }
}