<?php

namespace Mmatweb\Neural\Interfaces;

interface SynapseInterface
{
    public function open(): void;

    public function close(): void;

    public function isOpen(): bool;

    public function onReceive(callable $make): void;

    public function receive(bool $signal): void;

    public function getLastSignal(): ?bool;
}
