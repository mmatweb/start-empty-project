<?php

namespace Mmatweb\Neural\Interfaces;

interface AxonInterface
{
    public function sendSignal(bool $signal): void;

    public function attachSynapses(SynapseInterface ...$synapses): void;

    public function detachSynapses(SynapseInterface ...$synapses): void;
}
