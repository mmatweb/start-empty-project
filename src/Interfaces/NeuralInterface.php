<?php

namespace Mmatweb\Neural\Interfaces;

interface NeuralInterface
{
    public function setLastActiveSynapse(SynapseInterface $synapse): void;

    public function getLastActiveSynapse(): ?SynapseInterface;
}
