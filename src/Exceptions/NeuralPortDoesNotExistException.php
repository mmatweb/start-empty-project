<?php

namespace Mmatweb\Neural\Exceptions;

class NeuralPortDoesNotExistException extends \Exception
{
}
