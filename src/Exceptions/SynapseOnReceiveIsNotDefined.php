<?php

namespace Mmatweb\Neural\Exceptions;

class SynapseOnReceiveIsNotDefined extends \Exception
{
}
