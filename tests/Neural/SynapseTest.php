<?php

namespace Mmatweb\Neural\Tests\Neural;

use Mmatweb\Neural\Interfaces\NeuralInterface;
use Mmatweb\Neural\Neural\Synapse;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @internal
 */
final class SynapseTest extends TestCase
{
    /** @var Synapse */
    private $synapse;
    /** @var NeuralInterface|ObjectProphecy */
    private $neural;

    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->neural = $this->prophesize(NeuralInterface::class);
        $this->synapse = new Synapse($this->neural->reveal());
    }

    public function testClose()
    {
        $this->synapse->close();
        $this->assertFalse($this->synapse->isOpen());
    }

    public function testOpen()
    {
        $this->synapse->open();
        $this->assertTrue($this->synapse->isOpen());
    }

    /**
     * @throws \Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined
     */
    public function testOnReceive()
    {
        $this->synapse->onReceive(function ($signal, $neural) {
            $this->assertTrue(true === $signal);
            $this->assertInstanceOf(NeuralInterface::class, $neural);
        });
        $this->synapse->receive(true);
    }

    /**
     * @throws \Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined
     */
    public function testGetLastSignal()
    {
        $this->assertNull($this->synapse->getLastSignal());
        $this->synapse->onReceive(function () {});
        $this->synapse->receive(true);
        $this->assertTrue($this->synapse->getLastSignal());
        $this->synapse->receive(false);
        $this->assertFalse($this->synapse->getLastSignal());
    }
}
