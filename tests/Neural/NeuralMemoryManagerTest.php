<?php

namespace Mmatweb\Neural\Tests\Neural;

use Mmatweb\Neural\Interfaces\NeuralInterface;
use Mmatweb\Neural\Neural\NeuralMemoryManager;
use Mmatweb\Neural\Neural\Synapse;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @internal
 */
final class NeuralMemoryManagerTest extends TestCase
{
    /** @var NeuralInterface|ObjectProphecy */
    private $neuralInterface;
    /** @var Synapse */
    private $synapse;

    protected function setUp(): void
    {
        parent::setUp();
        $this->neuralInterface = $this->prophesize(NeuralInterface::class);
        $this->synapse = new Synapse($this->neuralInterface->reveal());
        $this->synapse->onReceive(function () {});
    }

    /** @throws \Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined */
    public function test memoryManger has not already learn()
    {
        $neuralMemoryManager = new NeuralMemoryManager();
        $neuralMemoryManager->getAxon1()->attachSynapses($this->synapse);

        $neuralMemoryManager->getSynapse1()->receive(true);
        $this->assertFalse($this->synapse->getLastSignal());

        $neuralMemoryManager->getSynapse1()->receive(false);
        $this->assertFalse($this->synapse->getLastSignal());
    }

    /** @throws \Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined */
    public function test memoryManger has learn()
    {
        $neuralMemoryManager = new NeuralMemoryManager();
        $neuralMemoryManager->getAxon2()->attachSynapses($this->synapse);
        $neuralMemoryManager->getAxon3()->attachSynapses($this->synapse);

        $neuralMemoryManager->getSynapse2()->receive(false);
        $neuralMemoryManager->getSynapse3()->receive(true);
        $neuralMemoryManager->getSynapse1()->receive(false);
        $this->assertTrue($this->synapse->getLastSignal());

        $neuralMemoryManager->getSynapse2()->receive(true);
        $neuralMemoryManager->getSynapse3()->receive(false);
        $neuralMemoryManager->getSynapse1()->receive(true);
        $this->assertFalse($this->synapse->getLastSignal());

        var_dump($neuralMemoryManager);
    }
}
