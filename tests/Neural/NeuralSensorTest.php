<?php

namespace Mmatweb\Neural\Tests\Neural;

use Mmatweb\Neural\Interfaces\NeuralInterface;
use Mmatweb\Neural\Neural\NeuralSensor;
use Mmatweb\Neural\Neural\Synapse;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * @internal
 */
final class NeuralSensorTest extends TestCase
{
    /** @var NeuralInterface|ObjectProphecy */
    private $neuralInterface;
    /** @var Synapse */
    private $synapse;

    protected function setUp(): void
    {
        parent::setUp();
        $this->neuralInterface = $this->prophesize(NeuralInterface::class);
        $this->synapse = new Synapse($this->neuralInterface->reveal());
        $this->synapse->onReceive(function () {});
    }

    /** @throws \Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined */
    public function test sensor return()
    {
        $neuralSensor = new NeuralSensor(true, false);
        $neuralSensor->getAxon1()->attachSynapses($this->synapse);

        $neuralSensor->getSynapse1()->receive(false);
        $this->assertTrue($this->synapse->getLastSignal());

        $neuralSensor->getSynapse1()->receive(true);
        $this->assertFalse($this->synapse->getLastSignal());
    }
}
