<?php

namespace Mmatweb\Neural\Tests\Network;

use Mmatweb\Neural\Interfaces\NeuralInterface;
use Mmatweb\Neural\Network\SimpleNetwork;
use Mmatweb\Neural\Neural\Synapse;
use PHPUnit\Framework\TestCase;

class SimpleNetworkTest extends TestCase
{
    private $neuralInterface;

    public function setUp(): void
    {
        parent::setUp();
        $this->neuralInterface = $this->prophesize(NeuralInterface::class);
    }

    /** @throws \Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined */
    public function test calibrate for return false()
    {
        $simpleNetwork = new SimpleNetwork(false, false);
        $synapse = new Synapse($this->neuralInterface->reveal());
        $synapse->onReceive(function () {});
        $simpleNetwork->getOutput()->attachSynapses($synapse);

        $simpleNetwork->getInput()->receive(false);
        $this->assertFalse($synapse->getLastSignal());

        $simpleNetwork->getInput()->receive(true);
        $this->assertFalse($synapse->getLastSignal());
    }

    /** @throws \Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined */
    public function test calibrate for return true()
    {
        $simpleNetwork = new SimpleNetwork(true, true);
        $synapse = new Synapse($this->neuralInterface->reveal());
        $synapse->onReceive(function () {});
        $simpleNetwork->getOutput()->attachSynapses($synapse);

        $simpleNetwork->getInput()->receive(false);
        $this->assertTrue($synapse->getLastSignal());

        $simpleNetwork->getInput()->receive(true);
        $this->assertTrue($synapse->getLastSignal());
    }

    /** @throws \Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined */
    public function test calibrate for return inverse()
    {
        $simpleNetwork = new SimpleNetwork(true, false);
        $synapse = new Synapse($this->neuralInterface->reveal());
        $synapse->onReceive(function () {});
        $simpleNetwork->getOutput()->attachSynapses($synapse);

        $simpleNetwork->getInput()->receive(false);
        $this->assertTrue($synapse->getLastSignal());

        $simpleNetwork->getInput()->receive(true);
        $this->assertFalse($synapse->getLastSignal());
    }

    /** @throws \Mmatweb\Neural\Exceptions\SynapseOnReceiveIsNotDefined */
    public function test calibrate for return same()
    {
        $simpleNetwork = new SimpleNetwork(false, true);
        $synapse = new Synapse($this->neuralInterface->reveal());
        $synapse->onReceive(function () {});
        $simpleNetwork->getOutput()->attachSynapses($synapse);

        $simpleNetwork->getInput()->receive(false);
        $this->assertFalse($synapse->getLastSignal());

        $simpleNetwork->getInput()->receive(true);
        $this->assertTrue($synapse->getLastSignal());
    }
}
